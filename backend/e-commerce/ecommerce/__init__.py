from flask import Flask

from .api_v1.views import api as api_v1
from .api_v2.views import api as api_v2
from .api_v3.views import api as api_v3


def create_app():
    app = Flask(__name__)

    app.register_blueprint(api_v1)
    app.register_blueprint(api_v2)
    app.register_blueprint(api_v3)

    return app

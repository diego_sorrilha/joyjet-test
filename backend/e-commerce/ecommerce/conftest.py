import json
import pytest

from ecommerce import create_app


@pytest.fixture(scope='module')
def test_client():
    flask_app = create_app()

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    yield testing_client

    ctx.pop()


# Tests to API Version 1.0
@pytest.fixture
def input_payload_v1():

    data = '''{
        "articles": [
            {"id": 1, "name": "water", "price": 100},
            {"id": 2, "name": "honey", "price": 200},
            {"id": 3, "name": "mango", "price": 400},
            {"id": 4, "name": "tea", "price": 1000}
        ],
        "carts": [
            {
                "id": 1,
                "items": [
                    {"article_id": 1, "quantity": 6},
                    {"article_id": 2, "quantity": 2},
                    {"article_id": 4, "quantity": 1}
                ]
            },
            {
                "id": 2,
                "items": [
                    {"article_id": 2, "quantity": 1},
                    {"article_id": 3, "quantity": 3}
                ]
            },
            {
                "id": 3,
                "items": []
            }
        ]
    }'''

    return json.loads(data)


@pytest.fixture
def input_payload_missing_articles_v1():

    data = '''{
        "carts": [
            {
                "id": 1,
                "items": [
                    {"article_id": 1, "quantity": 6},
                    {"article_id": 2, "quantity": 2},
                    {"article_id": 4, "quantity": 1}
                ]
            },
            {
                "id": 2,
                "items": [
                    {"article_id": 2, "quantity": 1},
                    {"article_id": 3, "quantity": 3}
                ]
            },
            {
                "id": 3,
                "items": []
            }
        ]
    }'''

    return json.loads(data)


@pytest.fixture
def expected_reponse_v1():

    data = '''{
      "carts": [
        {
          "id": 1,
          "total": 2000
        },
        {
          "id": 2,
          "total": 1400
        },
        {
          "id": 3,
          "total": 0
        }
      ]
    }'''

    return json.loads(data)


# Tests to API Version 2.0
@pytest.fixture
def input_payload_v2():

    data = '''{
      "articles": [
        { "id": 1, "name": "water", "price": 100 },
        { "id": 2, "name": "honey", "price": 200 },
        { "id": 3, "name": "mango", "price": 400 },
        { "id": 4, "name": "tea", "price": 1000 }
      ],
      "carts": [
        {
          "id": 1,
          "items": [
            { "article_id": 1, "quantity": 6 },
            { "article_id": 2, "quantity": 2 },
            { "article_id": 4, "quantity": 1 }
          ]
        },
        {
          "id": 2,
          "items": [
            { "article_id": 2, "quantity": 1 },
            { "article_id": 3, "quantity": 3 }
          ]
        },
        {
          "id": 3,
          "items": []
        }
      ],
      "delivery_fees": [
        {
          "eligible_transaction_volume": {
            "min_price": 0,
            "max_price": 1000
          },
          "price": 800
        },
        {
          "eligible_transaction_volume": {
            "min_price": 1000,
            "max_price": 2000
          },
          "price": 400
        },
        {
          "eligible_transaction_volume": {
            "min_price": 2000,
            "max_price": null
          },
          "price": 0
        }
      ]
    }'''

    return json.loads(data)


@pytest.fixture
def input_payload_missing_articles_v2():

    data = '''{
      "articles": [
        { "id": 1, "name": "water", "price": 100 },
        { "id": 2, "name": "honey", "price": 200 },
        { "id": 3, "name": "mango", "price": 400 },
        { "id": 4, "name": "tea", "price": 1000 }
      ],
      "carts": [
        {
          "id": 1,
          "items": [
            { "article_id": 1, "quantity": 6 },
            { "article_id": 2, "quantity": 2 },
            { "article_id": 4, "quantity": 1 }
          ]
        },
        {
          "id": 2,
          "items": [
            { "article_id": 2, "quantity": 1 },
            { "article_id": 3, "quantity": 3 }
          ]
        },
        {
          "id": 3,
          "items": []
        }
      ],
      "delivery_fees": [
        {
          "eligible_transaction_volume": {
            "min_price": 0,
            "max_price": 1000
          },
          "price": 800
        },
        {
          "eligible_transaction_volume": {
            "min_price": 1000,
            "max_price": 2000
          },
          "price": 400
        },
        {
          "eligible_transaction_volume": {
            "min_price": 2000,
            "max_price": null
          },
          "price": 0
        }
      ]
    }'''

    return json.loads(data)


@pytest.fixture
def input_payload_missing_delivery_fees_v2():

    data = '''{
      "articles": [
        { "id": 1, "name": "water", "price": 100 },
        { "id": 2, "name": "honey", "price": 200 },
        { "id": 3, "name": "mango", "price": 400 },
        { "id": 4, "name": "tea", "price": 1000 }
      ],
      "carts": [
        {
          "id": 1,
          "items": [
            { "article_id": 1, "quantity": 6 },
            { "article_id": 2, "quantity": 2 },
            { "article_id": 4, "quantity": 1 }
          ]
        },
        {
          "id": 2,
          "items": [
            { "article_id": 2, "quantity": 1 },
            { "article_id": 3, "quantity": 3 }
          ]
        },
        {
          "id": 3,
          "items": []
        }
      ]
    }'''

    return json.loads(data)


@pytest.fixture
def expected_reponse_v2():

    data = '''{
      "carts": [
        {
          "id": 1,
          "total": 2000
        },
        {
          "id": 2,
          "total": 1800
        },
        {
          "id": 3,
          "total": 800
        }
      ]
    }'''

    return json.loads(data)


# Tests to API Version 3.0
@pytest.fixture
def input_payload_v3():

    data = '''{
      "articles": [
        { "id": 1, "name": "water", "price": 100 },
        { "id": 2, "name": "honey", "price": 200 },
        { "id": 3, "name": "mango", "price": 400 },
        { "id": 4, "name": "tea", "price": 1000 },
        { "id": 5, "name": "ketchup", "price": 999 },
        { "id": 6, "name": "mayonnaise", "price": 999 },
        { "id": 7, "name": "fries", "price": 378 },
        { "id": 8, "name": "ham", "price": 147 }
      ],
      "carts": [
        {
          "id": 1,
          "items": [
            { "article_id": 1, "quantity": 6 },
            { "article_id": 2, "quantity": 2 },
            { "article_id": 4, "quantity": 1 }
          ]
        },
        {
          "id": 2,
          "items": [
            { "article_id": 2, "quantity": 1 },
            { "article_id": 3, "quantity": 3 }
          ]
        },
        {
          "id": 3,
          "items": [
            { "article_id": 5, "quantity": 1 },
            { "article_id": 6, "quantity": 1 }
          ]
        },
        {
          "id": 4,
          "items": [
            { "article_id": 7, "quantity": 1 }
          ]
        },
        {
          "id": 5,
          "items": [
            { "article_id": 8, "quantity": 3 }
          ]
        }
      ],
      "delivery_fees": [
        {
          "eligible_transaction_volume": {
            "min_price": 0,
            "max_price": 1000
          },
          "price": 800
        },
        {
          "eligible_transaction_volume": {
            "min_price": 1000,
            "max_price": 2000
          },
          "price": 400
        },
        {
          "eligible_transaction_volume": {
            "min_price": 2000,
            "max_price": null
          },
          "price": 0
        }
      ],
      "discounts": [
        { "article_id": 2, "type": "amount", "value": 25 },
        { "article_id": 5, "type": "percentage", "value": 30 },
        { "article_id": 6, "type": "percentage", "value": 30 },
        { "article_id": 7, "type": "percentage", "value": 25 },
        { "article_id": 8, "type": "percentage", "value": 10 }
      ]
    }'''

    return json.loads(data)


@pytest.fixture
def input_payload_missing_articles_v3():
    data = '''{
          "carts": [
            {
              "id": 1,
              "items": [
                { "article_id": 1, "quantity": 6 },
                { "article_id": 2, "quantity": 2 },
                { "article_id": 4, "quantity": 1 }
              ]
            },
            {
              "id": 2,
              "items": [
                { "article_id": 2, "quantity": 1 },
                { "article_id": 3, "quantity": 3 }
              ]
            },
            {
              "id": 3,
              "items": [
                { "article_id": 5, "quantity": 1 },
                { "article_id": 6, "quantity": 1 }
              ]
            },
            {
              "id": 4,
              "items": [
                { "article_id": 7, "quantity": 1 }
              ]
            },
            {
              "id": 5,
              "items": [
                { "article_id": 8, "quantity": 3 }
              ]
            }
          ],
          "delivery_fees": [
            {
              "eligible_transaction_volume": {
                "min_price": 0,
                "max_price": 1000
              },
              "price": 800
            },
            {
              "eligible_transaction_volume": {
                "min_price": 1000,
                "max_price": 2000
              },
              "price": 400
            },
            {
              "eligible_transaction_volume": {
                "min_price": 2000,
                "max_price": null
              },
              "price": 0
            }
          ],
          "discounts": [
            { "article_id": 2, "type": "amount", "value": 25 },
            { "article_id": 5, "type": "percentage", "value": 30 },
            { "article_id": 6, "type": "percentage", "value": 30 },
            { "article_id": 7, "type": "percentage", "value": 25 },
            { "article_id": 8, "type": "percentage", "value": 10 }
          ]
        }'''

    return json.loads(data)


@pytest.fixture
def input_payload_missing_delivery_fees_v3():
    data = '''{
          "articles": [
            { "id": 1, "name": "water", "price": 100 },
            { "id": 2, "name": "honey", "price": 200 },
            { "id": 3, "name": "mango", "price": 400 },
            { "id": 4, "name": "tea", "price": 1000 },
            { "id": 5, "name": "ketchup", "price": 999 },
            { "id": 6, "name": "mayonnaise", "price": 999 },
            { "id": 7, "name": "fries", "price": 378 },
            { "id": 8, "name": "ham", "price": 147 }
          ],
          "carts": [
            {
              "id": 1,
              "items": [
                { "article_id": 1, "quantity": 6 },
                { "article_id": 2, "quantity": 2 },
                { "article_id": 4, "quantity": 1 }
              ]
            },
            {
              "id": 2,
              "items": [
                { "article_id": 2, "quantity": 1 },
                { "article_id": 3, "quantity": 3 }
              ]
            },
            {
              "id": 3,
              "items": [
                { "article_id": 5, "quantity": 1 },
                { "article_id": 6, "quantity": 1 }
              ]
            },
            {
              "id": 4,
              "items": [
                { "article_id": 7, "quantity": 1 }
              ]
            },
            {
              "id": 5,
              "items": [
                { "article_id": 8, "quantity": 3 }
              ]
            }
          ],
          "discounts": [
            { "article_id": 2, "type": "amount", "value": 25 },
            { "article_id": 5, "type": "percentage", "value": 30 },
            { "article_id": 6, "type": "percentage", "value": 30 },
            { "article_id": 7, "type": "percentage", "value": 25 },
            { "article_id": 8, "type": "percentage", "value": 10 }
          ]
        }'''

    return json.loads(data)


@pytest.fixture
def input_payload_missing_discounts_v3():

    data = '''{
      "articles": [
        { "id": 1, "name": "water", "price": 100 },
        { "id": 2, "name": "honey", "price": 200 },
        { "id": 3, "name": "mango", "price": 400 },
        { "id": 4, "name": "tea", "price": 1000 },
        { "id": 5, "name": "ketchup", "price": 999 },
        { "id": 6, "name": "mayonnaise", "price": 999 },
        { "id": 7, "name": "fries", "price": 378 },
        { "id": 8, "name": "ham", "price": 147 }
      ],
      "carts": [
        {
          "id": 1,
          "items": [
            { "article_id": 1, "quantity": 6 },
            { "article_id": 2, "quantity": 2 },
            { "article_id": 4, "quantity": 1 }
          ]
        },
        {
          "id": 2,
          "items": [
            { "article_id": 2, "quantity": 1 },
            { "article_id": 3, "quantity": 3 }
          ]
        },
        {
          "id": 3,
          "items": [
            { "article_id": 5, "quantity": 1 },
            { "article_id": 6, "quantity": 1 }
          ]
        },
        {
          "id": 4,
          "items": [
            { "article_id": 7, "quantity": 1 }
          ]
        },
        {
          "id": 5,
          "items": [
            { "article_id": 8, "quantity": 3 }
          ]
        }
      ],
      "delivery_fees": [
        {
          "eligible_transaction_volume": {
            "min_price": 0,
            "max_price": 1000
          },
          "price": 800
        },
        {
          "eligible_transaction_volume": {
            "min_price": 1000,
            "max_price": 2000
          },
          "price": 400
        },
        {
          "eligible_transaction_volume": {
            "min_price": 2000,
            "max_price": null
          },
          "price": 0
        }
      ]
    }'''

    return json.loads(data)


@pytest.fixture
def expected_reponse_v3():

    data = '''{
      "carts": [
        {
          "id": 1,
          "total": 2350
        },
        {
          "id": 2,
          "total": 1775
        },
        {
          "id": 3,
          "total": 1798
        },
        {
          "id": 4,
          "total": 1083
        },
        {
          "id": 5,
          "total": 1196
        }
      ]
    }'''

    return json.loads(data)

import math

from ecommerce.logger import logger


def validate_parameter(parameter: dict) -> bool:
    logger.debug(f'Received parameters: {parameter}')
    is_valid = False

    if parameter:
        is_valid = all([
            parameter.get('articles'),
            parameter.get('carts'),
            parameter.get('delivery_fees')
        ])

    if is_valid:
        logger.debug('Parameters valid')
    else:
        logger.error('Parameters NOT valid')

    return is_valid


def get_article_by_id(parameter: dict, _id: int) -> dict:
    filter_by_id = (lambda x: x['id'] == _id)
    result = list(filter(filter_by_id, parameter['articles']))

    if result:
        return result[0]


def get_price_list(parameter: dict, items: list) -> list:
    price_list = []

    for item in items:
        article = get_article_by_id(parameter, item["article_id"])
        article_price = article['price']
        item_quantity = item['quantity']
        price_list.append(article_price * item_quantity)

    return price_list


def get_delivery_fee(parameter: dict, total: int) -> int:
    for fee in parameter['delivery_fees']:
        range = fee["eligible_transaction_volume"]
        value_min = range.get("min_price")
        value_max = range.get("max_price")
        min_price = value_min if value_min else 0
        max_price = value_max if value_max else math.inf

        if min_price <= total < max_price:
            return fee["price"]

    return 0


def make_cart_checkout(parameter: dict) -> dict:
    response = {'carts': []}

    for cart in parameter['carts']:
        response_data = {"id": cart["id"]}
        items = cart["items"]
        price_list = get_price_list(parameter, items)

        response_data['total'] = sum(price_list)

        response_data["total"] += get_delivery_fee(
            parameter,
            response_data["total"]
        )

        logger.info(f'Cart processed with success- ID: {response_data["id"]}')
        logger.debug(f'item: {response_data}')

        response['carts'].append(response_data)

    logger.info("All carts processed with success")
    logger.debug(f"'Carts': {response}")

    return response

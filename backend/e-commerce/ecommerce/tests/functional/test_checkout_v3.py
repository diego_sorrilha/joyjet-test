import json


def test_checkout_success_status_code(test_client, input_payload_v3):
    """
    GIVE a Flask application configured for testing
    WHEN the '/api/v3/checkout' is requested (POST)
    THEN check that the response is valid (status code = 200)
    """

    response = test_client.post('/api/v3/checkout', json=input_payload_v3)

    assert response.status_code == 200


def test_checkout_success_reponse(test_client, input_payload_v3, expected_reponse_v3):
    """
    GIVE a Flask application configured for testing
    WHEN the '/api/v3/checkout' is requested (POST)
    THEN check that the response is equals to expected
    """

    response = test_client.post('/api/v3/checkout', json=input_payload_v3)

    assert json.loads(response.data) == expected_reponse_v3


def test_checkout_method_error_status_code(test_client):
    """
    GIVE a Flask application configured for testing
    WHEN the '/api/v3/checkout' is requested (GET)
    THEN check is the response is Not Allowed (status code = 405)
    """
    response = test_client.get('/api/v3/checkout')

    assert response.status_code == 405


def test_checkout_no_payload_error_status_code(test_client):
    """
    GIVE a Flask application configured for testing
    WHEN the '/api/v3/checkout' is requested (POST) without payload
    THEN check is the response is Bad Request (status code = 400)
    """
    response = test_client.post('/api/v3/checkout')

    assert response.status_code == 400


def test_checkout_payload_missing_articles_error_status_code(test_client, input_payload_missing_articles_v3):
    """
    GIVE a Flask application configured for testing
    WHEN the '/api/v3/checkout' is requested (POST) with payload missing articles
    THEN check is the response is Bad Request (status code = 400)
    """
    response = test_client.post('/api/v3/checkout')

    assert response.status_code == 400


def test_checkout_payload_missing_delivery_fees_error_status_code(test_client, input_payload_missing_delivery_fees_v3):
    """
    GIVE a Flask application configured for testing
    WHEN the '/api/v3/checkout' is requested (POST) with payload missing delivery_fees
    THEN check is the response is Bad Request (status code = 400)
    """
    response = test_client.post('/api/v3/checkout')

    assert response.status_code == 400


def test_checkout_payload_missing_discounts_error_status_code(test_client, input_payload_missing_discounts_v3):
    """
    GIVE a Flask application configured for testing
    WHEN the '/api/v3/checkout' is requested (POST) with payload missing discounts
    THEN check is the response is Bad Request (status code = 400)
    """
    response = test_client.post('/api/v3/checkout')

    assert response.status_code == 400

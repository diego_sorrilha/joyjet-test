from ecommerce.logger import logger


def validate_parameter(parameter: dict) -> bool:
    logger.debug(f'Received parameters: {parameter}')
    is_valid = False

    if parameter:
        is_valid = all([parameter.get('articles'), parameter.get('carts')])

    if is_valid:
        logger.debug('Parameters valid')
    else:
        logger.error('Parameters NOT valid')

    return is_valid


def get_article_by_id(parameter: dict, _id: int) -> dict:
    filter_by_id = (lambda x: x['id'] == _id)
    result = list(filter(filter_by_id, parameter['articles']))

    if result:
        return result[0]


def get_price_list(parameter: dict, items: list) -> list:
    price_list = []

    for item in items:
        article = get_article_by_id(parameter, item["article_id"])
        article_price = article['price']
        item_quantity = item['quantity']
        price_list.append(article_price * item_quantity)

    return price_list


def make_cart_checkout(parameter: dict) -> dict:
    response = {'carts': []}

    for cart in parameter['carts']:
        response_data = {"id": cart["id"]}
        items = cart["items"]
        price_list = get_price_list(parameter, items)

        response_data['total'] = sum(price_list)

        logger.info(f'Cart processed with success- ID: {response_data["id"]}')
        logger.debug(f'item: {response_data}')

        response['carts'].append(response_data)

    logger.info("All carts processed with success")
    logger.debug(f"'Carts': {response}")

    return response

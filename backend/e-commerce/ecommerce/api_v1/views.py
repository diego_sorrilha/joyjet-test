from flask import Blueprint, jsonify, request

from ecommerce.api_v1.services.checkout import make_cart_checkout, validate_parameter

api = Blueprint('api', __name__, url_prefix='/api/v1')


@api.route('/checkout', methods=['POST'])
def index():
    parameter = request.json

    is_parameter_valid = validate_parameter(parameter)

    if is_parameter_valid:
        response = make_cart_checkout(parameter)

        return jsonify(response)

    return jsonify('Invalid Parameters'), 400

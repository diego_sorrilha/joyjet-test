import logging
import sys

handler = logging.StreamHandler(sys.stdout)
handler.setFormatter(
    logging.Formatter('[%(asctime)s] %(levelname)s in %(module)s: %(message)s')
)
logger = logging.getLogger()
logger.addHandler(handler)
logger.setLevel(logging.DEBUG)


E-Commerce API
====

## Endpoint overview

Version  | Endpoint         |  Description 
------------- |------------------| -------------
1.0  | /api/v1/checkout | Responsible for calculating checkout
2.0  | /api/v2/checkout | Responsible for calculating checkout with delivery fees
3.0  | /api/v3/checkout | Responsible for calculating checkout with delivery fees and discounts 

## Setup

Create a new virtualenv and activate it
```bash
python3 -m venv .venv && source .venv/bin/activate
```

With a virtualenv activated install the dependencies
```bash
pip3 install --upgrade pip && pip3 install -r requirements.txt
```

## Running tests and ensure PEP8

```bash
tox
```

## Running the API

```bash
export FLASK_APP=ecommerce

flask run
```

or use Makefile
```commandline
make run
```

### API Version 1 - Simple checkout

Use cURL:

```bash
curl --location --request POST 'http://localhost:5000/api/v1/checkout' \
--header 'Content-Type: application/json' \
--data-raw '{
        "articles": [
            {"id": 1, "name": "water", "price": 100},
            {"id": 2, "name": "honey", "price": 200},
            {"id": 3, "name": "mango", "price": 400},
            {"id": 4, "name": "tea", "price": 1000}
        ],
        "carts": [
            {
                "id": 1,
                "items": [
                    {"article_id": 1, "quantity": 6},
                    {"article_id": 2, "quantity": 2},
                    {"article_id": 4, "quantity": 1}
                ]
            },
            {
                "id": 2,
                "items": [
                    {"article_id": 2, "quantity": 1},
                    {"article_id": 3, "quantity": 3}
                ]
            },
            {
                "id": 3,
                "items": []
            }
        ]
    }'
```

### API Version 2 - Checkout calculating delivery fees

Use cURL:

```bash
curl --location --request POST 'http://localhost:5000/api/v2/checkout' \
--header 'Content-Type: application/json' \
--data-raw '{
  "articles": [
    { "id": 1, "name": "water", "price": 100 },
    { "id": 2, "name": "honey", "price": 200 },
    { "id": 3, "name": "mango", "price": 400 },
    { "id": 4, "name": "tea", "price": 1000 }
  ],
  "carts": [
    {
      "id": 1,
      "items": [
        { "article_id": 1, "quantity": 6 },
        { "article_id": 2, "quantity": 2 },
        { "article_id": 4, "quantity": 1 }
      ]
    },
    {
      "id": 2,
      "items": [
        { "article_id": 2, "quantity": 1 },
        { "article_id": 3, "quantity": 3 }
      ]
    },
    {
      "id": 3,
      "items": []
    }
  ],
  "delivery_fees": [
    {
      "eligible_transaction_volume": {
        "min_price": 0,
        "max_price": 1000
      },
      "price": 800
    },
    {
      "eligible_transaction_volume": {
        "min_price": 1000,
        "max_price": 2000
      },
      "price": 400
    },
    {
      "eligible_transaction_volume": {
        "min_price": 2000,
        "max_price": null
      },
      "price": 0
    }
  ]
}'
```
### API Version 3 - Checkout calculating delivery fees and discounts

Use cURL:

```bash
curl --location --request POST 'http://localhost:5000/api/v3/checkout' \
--header 'Content-Type: application/json' \
--data-raw '{
  "articles": [
    { "id": 1, "name": "water", "price": 100 },
    { "id": 2, "name": "honey", "price": 200 },
    { "id": 3, "name": "mango", "price": 400 },
    { "id": 4, "name": "tea", "price": 1000 },
    { "id": 5, "name": "ketchup", "price": 999 },
    { "id": 6, "name": "mayonnaise", "price": 999 },
    { "id": 7, "name": "fries", "price": 378 },
    { "id": 8, "name": "ham", "price": 147 }
  ],
  "carts": [
    {
      "id": 1,
      "items": [
        { "article_id": 1, "quantity": 6 },
        { "article_id": 2, "quantity": 2 },
        { "article_id": 4, "quantity": 1 }
      ]
    },
    {
      "id": 2,
      "items": [
        { "article_id": 2, "quantity": 1 },
        { "article_id": 3, "quantity": 3 }
      ]
    },
    {
      "id": 3,
      "items": [
        { "article_id": 5, "quantity": 1 },
        { "article_id": 6, "quantity": 1 }
      ]
    },
    {
      "id": 4,
      "items": [
        { "article_id": 7, "quantity": 1 }
      ]
    },
    {
      "id": 5,
      "items": [
        { "article_id": 8, "quantity": 3 }
      ]
    }
  ],
  "delivery_fees": [
    {
      "eligible_transaction_volume": {
        "min_price": 0,
        "max_price": 1000
      },
      "price": 800
    },
    {
      "eligible_transaction_volume": {
        "min_price": 1000,
        "max_price": 2000
      },
      "price": 400
    },
    {
      "eligible_transaction_volume": {
        "min_price": 2000,
        "max_price": null
      },
      "price": 0
    }
  ],
  "discounts": [
    { "article_id": 2, "type": "amount", "value": 25 },
    { "article_id": 5, "type": "percentage", "value": 30 },
    { "article_id": 6, "type": "percentage", "value": 30 },
    { "article_id": 7, "type": "percentage", "value": 25 },
    { "article_id": 8, "type": "percentage", "value": 10 }
  ]
}'
```